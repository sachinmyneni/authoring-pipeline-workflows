# Pipeline details for this project

The Pipelines in this project are for a variety of differing scenarios, intended to show that complex pipelines can be combined for a differing exclusive scenarios.  They are still a WIP.

Pushes to production are intended to be run from tags that use a specific naming convention (ie: release.xx.xx.xx where x is an integer).  However, in emergencies they can be run directly from a push to the default branch, or a merge commit in the default branch.  All releases to the production environment are configured to require approval before the manual job can run.  Other tags that do not conform to this naming convention will get the full suite of tests with a preview environment.

This team wants to be able to trigger manual/API/scheduled pipelines either of a standard form for any branch/tag, or from a custom list of jobs they can put into the variable `JOB_SELECTIONS` as either a space or comma delimited list of job names available from the list in [pipelines/all-jobs.yml](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/ci-cd-examples/authoring-pipeline-workflows/-/blob/main/pipelines/all-jobs.yml).

The security team needs to be able to audit any branch or tag on a regular basis.  They prefer to schedule these tests on supported past releases (tags), and the main branch.

They also want to be able to trigger `coding-standards` and `code-quality` tests from a mention in commit messages in any branch other than the default branch.  For this scenario, they can put the string `CODE_TESTS` anywhere in the commit message.



## Workflows
- Commits to the main branch (default branch) will use a pipeline that runs a build and all tests with full deploy and revert capabilities.  Deploy jobs are manual and require approvals to be able to run.  These deploy jobs for commits in the default branch are there for emergency situations.
- Merge Requests targeted to the main branch will also run a build and all tests and provide a preview environment.  This project is configured to use pre-merged results for pipelines in MRs.
- Merge Commits going into the main branch will allow for a deployment to production that is manually activated for emergencies, and a revert deploy job that is also manually activated.  Deploy jobs require approvals before they can be run.  This project is configured to use merge commits and to not use fast forward commits for merge requests.
- Tags that have names that comply with naming conventions for a release will have a pipeline at creation time that uses the a build job with a full suite of tests, include a preview environment, DAST testing, as well as manual deploy and revert jobs.
- Tags that do not comply with the release naming conventions will get a build, and all tests except DAST.
- Security testing can be executed on any branch or tag, via the Manual pipelines page, API, or scheduled.
- Developers can execute quick code tests from any brain on than main, by adding the word CODE_TESTS to their commit message.
- Pipelines can be custom assembled from the list of jobs in [pipelines/select-jobs.yml](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/ci-cd-examples/authoring-pipeline-workflows/-/blob/main/pipelines/select-jobs.yml) and executed from the web-based Run Pipeline page, API, or via Scheduled Jobs.

## Files contained in this directory

Two files `rules.yml` and `all-jobs.yml` are not pipeline workflow files.  They both define rules or jobs that can be referenced elsewhere in real pipelines.  Rules defined in `rules.yml` are attached to hidden jobs that can either be extended in downstream jobs, or referenced usinf reference syntax which is the approach I am using here.  Jobs defined in `all-jobs.yml` are ctreate with a single rule that is when:never, so the declaration of jobs via file inclusion will never allow them to run until they are defined in a workflow file and a working rule referenced.  

Pipeline workflow yml files in this project declare the job names they will use, then either reference or hard code functional rules that will match that file's inclusion rule/rules from  gitlab-ci.yml.  This approach allows for jobs to always be available to be used, the choice of which ones to use falling to the individual pipeline workflow file that gets included from .gitlab-ci.yml.

The files and their descriptions follow:
- 'all-jobs.yml' is intended to have all potential jobs defined in ready to go mode.  They all use a single rule 'when: never' to prohibit their being able to create a job from the unconditional inclusion of this file.
- 'default-branch-commit.yml' is the pipeline used for non merge commits going into the default branch (main on this project).
- 'default-branch-merge-commit.yml' is is used for merge commits coming into the default branch.  This project does not use fast forward merge commits (which wont work with the job rules for this pipeline).
- 'default-branch-mr.yml' is used for merge requests targeted to the default branch.
- 'quick-code-tests.yml' allows developers to execute quick code tests on commits to branches other than main.
- 'release-tag.yml' is used for specifically named tags that conform to release tags for this theoretical project.
- 'rules.yml' is a list of rules that are referenced in jobs instantiated in pipeline workflow files.  It is uncondtionally included.
- 'security-suite.yml' is used to run security tests/audits on any branch or tag.
- 'select-jobs.yml' allows the contributors using this project to assemble a list of jobs at run time (custom pipeline) using the web UI, API, or a Scheduled Pipeline.
- 'tag.yml' is used for tags that do not have conforming names for release tags.
